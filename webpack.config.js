var path = require('path');
var webpack = require('webpack');

const fs = require('fs-extra');
const entry = {};
const javascriptEntry = {};

var config = {
    page: {
        //程序入口
        introduce: ["04.views","introduce.vue"], //app 介绍
        entry: ["04.views","entry.vue"],
        ranking: ["04.views","ranking.vue"],
        
        //导航页面
        index: ["04.views","Index.vue"],
        subscribe: ["04.views","subscribe.vue"],
        message: ["04.views","message.vue"],
        libraryBook: ["04.views","libraryBook.vue"],
        me: ["04.views","me.vue"],


        // 图书馆
        librarySearch: ["04.views","librarySearch.vue"],
        librarySearchResault: ["04.views","librarySearchResault.vue"],  //书籍搜索结果
        libraryBookListDetail: ["04.views","libraryBookListDetail.vue"],     //书单详细介绍
        libraryBookIntroduce: ["04.views","libraryBookIntroduce.vue"],  //书籍介绍
        libraryBookShelf: ["04.views","libraryBookShelf.vue"],
        
        //文章专辑，文章详细
        articleAlbumInfo: ["04.views","articleAlbumInfo.vue"],  //专辑下的所有文章
        articleDetails: ["04.views","articleDetails.vue"],    //文章阅读
        articleAlbumAll: ["04.views","articleAlbumAll.vue"],  //所有专辑
        
        searchPage: ["04.views","SearchPage.vue"],

        //个人中心
        mySubscribe: ["04.views","mySubscribe.vue"],
        myArticles: ["04.views","myArticles.vue"],
        myBooks: ["04.views","myBooks.vue"],
        aboutUs: ["04.views","aboutUs.vue"],
        aboutWeXin: ["04.views","aboutWeXin.vue"],
        aboutCheckIn: ["04.views","aboutCheckIn.vue"],
        
        // 商店入口，商店分类，商店中书籍详细信息
        storeCategory: ["04.views","storeCategory.vue"],
        storeBookDetail:["04.views","storeBookDetail.vue"],
        
        // 用户登录，注册，找回密码,反馈 等
        userLogin: ["04.views","userLogin.vue"],
        userRegisterByEmail: ["04.views","userRegisterByEmail.vue"],
        userRegisterByPhone: ["04.views","userRegisterByPhone.vue"],
        userGetPswBackByEmail: ["04.views","userGetPswBackByEmail.vue"],
        userGetPswBackByPhone: ["04.views","userGetPswBackByPhone.vue"],
        userResetPsw:["04.views","userResetPsw.vue"],
        userFeedback: ["04.views","userFeedback.vue"],
        userPersonalProfile: ["04.views","userPersonalProfile.vue"],
        userDonate: ["04.views","userDonate.vue"], //用户奉献
    },
    components:{
        "storeEntry": ["03.components","libraryStore.vue"],
        "LoadingDots": ["03.components","defaultLoading.vue"],
        "videoPlay": ["03.components","videoPlay.vue"],
        "webView": ["03.components","weexWebView.vue"],
        "networkError": ["03.components","networkErrorDisplay.vue"],
        "topbar": ["03.components","topbar.vue"],
        "commonFun": ["03.components","commonFun.vue"],
        "weextabbar": ["03.components","weextabbar.vue"],
        "libraryNav": ["03.components","libraryNav.vue"],
        "libraryBookShelf": ["03.components","libraryBookShelf.vue"], 
        "searchBar": ["03.components","searchBar.vue"],
        "netError": ["03.components","netError.vue"],
        "loadingCircle": ["03.components","loadingCircle.vue"],
        "progress": ["03.components","progress.vue"],
        "weexDialogue": ["03.components","weexDialogue.vue"],
        "weexOverlay": ["03.components","weexOverlay.vue"],
        "defaultImage": ["03.components","defaultImage.vue"],
        "submitting": ["03.components","submitting.vue"],
        "chat": ["03.components","chat.vue"],
        "libraryBook": ["03.components","libraryBook.vue"],
        "libraryCategory": ["03.components","libraryCategory.vue"],
        "libraryBookLists": ["03.components","libraryBookLists.vue"],
        "crIconFont": ["03.components","crIconFont.vue"],
        "topNavigationWidget": ["03.components","topNavigationWidget.vue"],
        "topNavigationWidgetWithBack": ["03.components","topNavigationWidgetWithBack.vue"],
        "followAlbum": ["03.components","followAlbum.vue"],
    }
}

var isWin = /^win/.test(process.platform);
let registerComponent = "";
function getComponentsInfo(){
    const vuedirectory =  path.join(__dirname, 'src');
    const webDirectory =  path.join(__dirname, 'web');
    console.log(webDirectory);
    let contents = '';
    for(let item in config.components){
        let component = config.components[item];
        let file = path.join(vuedirectory,component[0],component[1]);
        let relativePath = path.relative(webDirectory, file);

        if (isWin) {
            relativePath = relativePath.replace(/\\/g,'\\\\');
        }

        contents += 'var '+item+' = require(\'' + relativePath + '\')\n';
        contents += 'Vue.component(\''+item+'\', '+item+')\n';
    }
    // console.log(contents);
    registerComponent = contents;
}

getComponentsInfo();

function getEntryFileContent(entryPath, vueFilePath) {
    let relativePath = path.relative(path.join(entryPath, '../'), vueFilePath);
    // console.log(path.join(entryPath, '../')+" -- "+vueFilePath);
    let contents = '';
    if (isWin) {
        relativePath = relativePath.replace(/\\/g,'\\\\');
    }

    contents += 'var App = require(\'' + relativePath + '\')\n';
    contents += registerComponent;
    contents += 'App.el = \'#root\'\n';
    contents += 'new Vue(App)\n';
    // contents += "import router from \'../src/02.bus/router\'\n";
    // contents += 'new Vue(Vue.util.extend({\n  el:\'#root\',\n  router,\n},App))';
    return contents;
}

function walk(dir) {
    dir = dir || '.';
    const vuedirectory =  path.join(__dirname, 'src');
    const webDirectory =  path.join(__dirname, 'web');
    const tempFile = path.join(__dirname, 'assets',"template.html");
    const reg=new RegExp("templatename","gm");
    var htmlContent = fs.readFileSync(tempFile,"utf-8");
    // console.log(htmlContent);
    for(let item in config.page){
      let page = config.page[item];
      let file = path.join(vuedirectory,page[0],page[1]);
      entry[item] = file;
      javascriptEntry[item] = path.join(webDirectory,item+".js");     
      let entryFile = path.join(webDirectory, item)+".js";
      fs.outputFileSync(entryFile.replace(".js",".html"), htmlContent.replace(reg,item+".web"));
      fs.outputFileSync(entryFile, getEntryFileContent(entryFile, file));
    }
}

walk();


var bannerPlugin = new webpack.BannerPlugin({
  banner: '// { "framework": "Vue" }\n',
  raw: true
})

// var android_assets = "/Users/vincent/DashBoard/110 - Work  工作/112 - 基督徒阅读/jdt-android/app/src/main/assets";
 var android_assets = "/users/james/200--Work/203--Ios/CR_IOS/CReader/bundlejs";

function getWebBaseConfig () {
  return {
    entry: javascriptEntry,
    output: {
      path: path.resolve(__dirname, 'dist'),
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/
        }, {
          test: /\.vue(\?[^?]+)?$/
        }
      ]
    },
    plugins: [
      // new webpack.optimize.UglifyJsPlugin({compress: { warnings: false }}),
      bannerPlugin
    ]
  }
}

function getNavBaseConfig () {
    return {
        entry: javascriptEntry,
        output: {
            path: path.resolve(__dirname, 'dist')
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/
                }, {
                    test: /\.vue(\?[^?]+)?$/
                }
            ]
        },
        plugins: [
            // new webpack.optimize.UglifyJsPlugin({compress: { warnings: false }}),
            bannerPlugin
        ]
    }
}

function getAndroidBaseConfig () {
    return {
        entry: javascriptEntry,
        output: {
            path: android_assets
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/
                }, {
                    test: /\.vue(\?[^?]+)?$/
                }
            ]
        },
        plugins: [
            // new webpack.optimize.UglifyJsPlugin({compress: { warnings: false }}),
            bannerPlugin
        ]
    }
}

var webConfig = getWebBaseConfig()
webConfig.output.filename = '[name].web.js'
webConfig.module.rules[1].loader = 'vue-loader'
/**
 * important! should use postTransformNode to add $processStyle for
 * inline style normalization.
 */
webConfig.module.rules[1].options = {
  compilerModules: [
    {
      postTransformNode: el => {
        el.staticStyle = `$processStyle(${el.staticStyle})`
        el.styleBinding = `$processStyle(${el.styleBinding})`
      }
    }
  ]
}

var nativeConfig = getNavBaseConfig()
nativeConfig.output.filename = '[name].js'
nativeConfig.module.rules[1].loader = 'weex-loader'

var androidConfig = getAndroidBaseConfig()
androidConfig.output.filename = '[name].js'
androidConfig.module.rules[1].loader = 'weex-loader'

module.exports = [webConfig, nativeConfig,androidConfig]
// module.exports = [webConfig]