package com.weex.app;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;


public class SplashActivity extends AppCompatActivity {


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);

    Intent intent = new Intent(SplashActivity.this, WXPageActivity.class);
    Uri data = getIntent().getData();
    if (data != null) {
      intent.setData(data);
    }
    intent.putExtra("from", "splash");
    startActivity(intent);
    finish();
  }
}
