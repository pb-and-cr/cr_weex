// const host = "http://file.biblemooc.net/";
// const host = "http://172.20.10.4:1099";
const host = "http://47.244.180.248:1099";
// const host = "http://192.168.2.127:12000";
// const host = "http://read.biblemooc.net";

export function getHost() {
    return host;
}

function getStream() {
    let stream = weex.requireModule('stream');
    return stream;
}

function paramsToString(obj) {
    var param = ""
    for (let name in obj) {
        param += "&" + name + "=" + obj[name]
    }
    return param.substring(1)
}

export function fetch(path, method, param, successFun) {
    path = host + path;
    getStream().fetch({
        method: method,
        url: path,
        type: 'json',
        body: paramsToString(param)
    }
        , successFun);
    console.log(path);
}

// export function  get(path,param,fun) {
//     console.log(host+path+"?"+paramsToString(param));
//     return getStream().fetch({
//         method: 'GET',
//         type: 'json',
//         url: host+path+"?"+paramsToString(param),
//     }, fun)
// }

export function get(path, param) {
    console.log(host + path + "?" + paramsToString(param));
    return new Promise((resolve, reject) => {
        getStream().fetch(
            {
                method: 'GET',
                url: host + path + "?" + paramsToString(param),
                type: 'json'
            },
            function (ret) {
                resolve(ret);
            }
        );
    });
}
//
// export function post(path,param,successFun)
// {
//     path = host+path;
//     console.log(path);
//     console.log(paramsToString(param));
//     getStream().fetch({
//             method: 'POST',
//             url: path,
//             type: 'json',
//             headers:{'Content-Type':'application/x-www-form-urlencoded'},
//             body: paramsToString(param)
//         }
//         ,successFun);
// }

export function post(path, param) {
    path = host + path;
    console.log(path);
    console.log(paramsToString(param));
    return new Promise((resolve, reject) => {
        getStream().fetch(
            {
                method: 'POST',
                url: path,
                type: 'json',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                body: paramsToString(param)
            },
            function (ret) {
                resolve(ret);
            }
        );
    });
}
