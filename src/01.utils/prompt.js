
let prompt = {

    getModel(){
        return weex.requireModule('modal');
    },

    toast(info){
        this.getModel().toast({ message: info, duration: 0.3});
    },
    alert(info){
    	this.getModel().alert({ message: info, duration: 0.3});
    }
}

export default prompt;