export function getScreanOrientation(img_name){
  let deviceInfo = weex.requireModule('deviceInfo');
  let orientation = 1; // 1 表示竖屏 0 表示横屏
  try {
    if (typeof(deviceInfo) != "undefined" && typeof(deviceInfo.GetScreanOrientation) == "function") {
      orientation = deviceInfo.GetScreanOrientation();
    }
  }
  catch (e) {}
  return orientation;
};

/**
 * 获得设备屏幕信息，包括 宽，高
 */
export function getDeviceDisplayInfo(){
  if(weex.config.env.platform == 'android'){
    // return WXEnvironment;   
    console.log(WXEnvironment);
    try {
      let deviceInfo = weex.requireModule('event');
      if (typeof(deviceInfo.getDisplay) != "undefined" && typeof(deviceInfo.getDisplay) == "function") {
        let displayInfo = JSON.parse(deviceInfo.getDisplay());
        // 返回的值是： {"deviceWidth":1080,"deviceHeight":2248}
        console.log(displayInfo);
        return displayInfo;
      }
      else{
        return WXEnvironment;   
      }
    }
    catch(e){
      console.log(e);
      return WXEnvironment; 
    }
  }
  else
  {
     return WXEnvironment;
  }
};
