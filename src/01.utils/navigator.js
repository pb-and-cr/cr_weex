import util from './util';
import prompt from "./prompt";

function getNavigator(){
    let nav =  weex.requireModule('navigator');
    return nav;
};

export function paramsToString(obj){
    var param = ""
    for(let name in obj) {
        param += "&" + name + "=" + encodeURI(obj[name])
    }
    return param.substring(1)
};

function getUrl(url,jsFile) {
    const bundleUrl = url;
    let host = '';
    let path = '';
    let nativeBase = '';
    const isWebAssets = bundleUrl.indexOf('http://') >= 0;
    const isAndroidAssets = bundleUrl.indexOf('file://assets/') >= 0;
    const isiOSAssets = bundleUrl.indexOf('file:///') >= 0 && bundleUrl.indexOf('CReader.app') > 0;
    if (isAndroidAssets) {
        nativeBase = 'file://assets/';
    }else if (isiOSAssets) {
        // file:///var/mobile/Containers/Bundle/Application/{id}/WeexDemo.app/
        // file:///Users/{user}/Library/Developer/CoreSimulator/Devices/{id}/data/Containers/Bundle/Application/{id}/WeexDemo.app/
        nativeBase = bundleUrl.substring(0, bundleUrl.lastIndexOf('/') + 1);
    } else {
        const matches = /\/\/([^\/]+?)\//.exec(bundleUrl);
        const matchFirstPath = /\/\/[^\/]+\/([^\/]+)\//.exec(bundleUrl);
        if (matches && matches.length >= 2) {
            host = matches[1];
        }
        if (matchFirstPath && matchFirstPath.length >= 2) {
            path = matchFirstPath[1];
        }
        nativeBase = 'http://' + host + '/';
    }
    const h5Base = './index.html?page=';
    // in Native
    let base = nativeBase;
    if (typeof navigator !== 'undefined' && (navigator.appCodeName === 'Mozilla' || navigator.product === 'Gecko')) {
        // check if in weexpack project
        if (path === 'web' || path === 'dist') {
            base = h5Base + '/dist/';
        } else {
            base = h5Base + '';
        }
    } else {
        base = nativeBase + (!!path? path+'/':'');
    }

    const newUrl = base + jsFile;
    return newUrl;
}
    // 跳转到新的url地址
export function jump(self,url) {
    if (WXEnvironment.platform == 'Web') {
        window.location.href = "/web/"+url+".html";
    }
    else
    {

        let path = self.$getConfig().bundleUrl;

        path = util.setWXBundleUrl(path, url+".js");
        // prompt.alert(path);
        getNavigator().push({
            url: url,
            animated: "true"
        });
    }
}

export function back(self){
    if (WXEnvironment.platform == 'Web') {
        window.history.go(-1)
    }
    else
    {
        getNavigator().pop({
            animated: "true"
        });
    }
}

export function getWithParameter(self,url,parameter) {
    if (WXEnvironment.platform == 'Web') {
        window.location.href = "/web/"+url+".html?phantom_limb=true&"+paramsToString(parameter);
    }
    else
    {
        let path = self.$getConfig().bundleUrl;
        path = getUrl(path, url+".js?"+paramsToString(parameter));
        var event = weex.requireModule('event');
        event.openURL(path);
    }
}

export function jumpSubPage(self,url,parameter) {
    if (WXEnvironment.platform == 'Web') {
        window.location.href = "/web/"+url+".html?phantom_limb=true&"+paramsToString(parameter);
    }
    else
    {
        var event = weex.requireModule('event');
        let path = self.$getConfig().bundleUrl;
        console.log(path+"qingsongtest0000000000");
        let h = path.includes('?');
        console.log(h);
        if(h){
            let questionIndex = path.indexOf("?");
            path = path.slice(0,questionIndex);
            path = getUrl(path, url+".js?isSubPage&"+paramsToString(parameter));
            event.openURL(path);
        }else{
            path = getUrl(path, url+".js?isSubPage&"+paramsToString(parameter));
            event.openURL(path);
        }
    }
}