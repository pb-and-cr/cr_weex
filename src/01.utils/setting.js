/**
 * Created by zwwill on 2017/8/27.
 */

let setting = {
    getPlatform(){
        return weex.config.env.platform.toLowerCase();
    },
    isWeb() {
        return this.getPlatform()=="web";
    },
    isAndroid() {
        return this.getPlatform()=="android";
    },
    isIOS() {
        return this.getPlatform()=="ios";
    },
};

export default setting;