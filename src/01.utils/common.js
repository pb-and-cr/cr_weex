import prompt from '../01.utils/prompt';

// 获取图片在三端上不同的路径
// e.g. 图片文件名是 test.jpg, 转换得到的图片地址为
// - H5      : http: //localhost:1337/src/images/test.jpg
// - Android : local:///test
// - iOS     : ../images/test.jpg
export function getImgPath(img_name) {
    let bundleUrl = weex.config.bundleUrl
    let platform = weex.config.env.platform
    let img_path = ''
    if (platform == 'Web' || bundleUrl.indexOf("http") >= 0) {
        img_path = `/assets/images/${img_name}`
    } else if (platform == 'android') {
        // android 不需要后缀
        img_name = img_name.substr(0, img_name.lastIndexOf('.'));
        img_path = `local:///${img_name}`
    } else {
        //ios
        img_path = `local:///bundlejs/images/${img_name}`;
    }
    return img_path
};

export function getIconFontPath() {
    let bundleUrl = weex.config.bundleUrl
    let platform = weex.config.env.platform
    let iconFont_path = '';
    // console.log("platform: "+platform);
    // console.log("bundleUrl: "+bundleUrl);
    if (platform == 'android') {
        // android 不需要后缀
        // console.log("platform: android -- ");
        iconFont_path = `local:///font/`;
    } else if (platform == 'iOS') {
        //ios 
        // iconFont_path =bundleUrl.substring(0, bundleUrl.lastIndexOf('/') + 1)+`font/`
        iconFont_path = `local:///bundlejs/font/`;
    }
    else {
        iconFont_path = `/assets/font/`;
    }
    // console.log("iconFont_path: "+iconFont_path);
    return iconFont_path
}

// 发送信息到 google analytics
export function logVideoPlayEvent(videoName, videoId, album) {
    if (WXEnvironment.platform != 'Web') {
        var event = weex.requireModule('event');
        if (typeof (event.logVideoPlayEvent) != "undefined") {
            event.logVideoPlayEvent(videoName, videoId, album);
        }
    }
};

export function checkNetStatusOnOrOff() {
    if (WXEnvironment.platform != 'Web') {
        try {
            var event = weex.requireModule('event');
            if (typeof (event.isNetworkAvailable) != "undefined") {
                return event.isNetworkAvailable();
            }
        }
        catch (e) {
            return 1;
        }
    }
    else {
        return 1;
    }
};

export function getTabBarHeight() {

    try {
        var event = weex.requireModule('event');
        // console.log("qingsongtabbar"+event.getTabBarHeightForDiffPhone);
        return event.getTabBarHeightForDiffPhone();
    } catch (e) {
        return 98;
    }
};
