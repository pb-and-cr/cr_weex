
const storage = weex.requireModule('storage');

export function storageSetItem(key,value){
    // console.log(' -- storageSetItem -- ');
    // console.log(value);
    return new Promise((resolve, reject) => {
        storage.setItem(key, value, event => {
            // console.log(event);
            resolve(event);
        })
    });
};


export function storageGetItem(key){
    return new Promise((resolve, reject) => {
        storage.getItem(key, event => {
            // console.log(" storage event ");
            // console.log(event);
            resolve(event);
        })
    });
}

export function storageRemoveItem(key){
    return new Promise((resolve, reject) => {
        storage.removeItem(key, event => {
            // console.log(event);
            resolve(event);
        })
    });
}