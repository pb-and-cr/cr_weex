/**
 * Created by zwwill on 2017/8/27.
 */

const EVENT_DOWNLOAD = 'downLoadBook';

let downloadEvent = {
    addDownLoadBookEventListener(fun) {
        let event = weex.requireModule('globalEvent');
        event.addEventListener(EVENT_DOWNLOAD, function (value) {
            console.log("get downLoadBook")
            fun(value);
        });
    },
    removeDownLoadBookEventListener() {
        let event = weex.requireModule('globalEvent');
        console.log("remove downLoadBook")
        event.removeEventListener(EVENT_DOWNLOAD);
    },
};

export default downloadEvent;