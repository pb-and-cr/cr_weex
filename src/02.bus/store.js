import { get, post } from '../01.utils/fetch'
import { getOpenId, getUserId } from '../02.bus/user'
import { storageSetItem, storageGetItem, storageRemoveItem } from '../01.utils/storage'
import global from '../02.bus/global'
import syncAblumInfo from '../02.bus/syncAblumInfo';
import prompt from '../01.utils/prompt';
import { jumpSubPage } from '../01.utils/navigator';
// import googleTrack from '../02.bus/googleTrack';

//获得商店新
export function getBookList() {
    return getOpenId().then(openId => {
        return get('/api/shop/getBooksInShop', { 'openId': openId });
    })
    // return get('/api/shop/getBooksInShop', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}

//根据书籍的标签获得信息
export function getBookListByTag(tagid) {
    return getOpenId().then(openId => {
        return get('/api/shop/getBooksInShopByTag', { 'openId': openId, 'tag_id': tagid });
    })
    // return get('/api/shop/getBooksInShop', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}

//获取书籍信息
export function getBookProfile(id) {
    return getOpenId().then(openId => {
        return get('/api/shop/getBookProfile', { 'openId': openId, 'id': id });
    })
}