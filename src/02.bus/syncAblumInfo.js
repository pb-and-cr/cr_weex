const sync = new BroadcastChannel('christian reader')

let syncAblumInfo = {
  receive(fun) {
    sync.onmessage = fun;
  },
  follow(ablumnId) {
    let message = {
      status:'add',
      id:ablumnId,
    }
    sync.postMessage(message);
  },
  cancel(ablumnId) {
    let message = {
      status:'cancel',
      id:ablumnId
    }
    sync.postMessage(message);
  },
  getData(){
    let message = {
      status:'getData',
    }
    sync.postMessage(message);
  },
  close(ablumnId,fun) {
    sync.close();
  }
};

export default syncAblumInfo;