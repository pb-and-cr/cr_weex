/**
 * Created by vincent on 2017/8/27.
 * 使用杜威的十进制来进行数据分析和处理
 */

// let googleTrack = {

//     USER_BEHAVIOR:'userBehavior',

//     USER_BEHAVIOR_HOME:'home',                      // 主页
//     USER_BEHAVIOR_ALBUM:'album',
//     USER_BEHAVIOR_ARTICLE:'albumArticleDetail',                    // 文章点击

//     USER_BEHAVIOR_LIBRARY:'library',                    // 图书馆
//     USER_BEHAVIOR_LIBRARY_BOOKSHELF:'libraryBookshelf',        // 图书馆-书架
//     USER_BEHAVIOR_LIBRARY_CATEGORY:'libraryCategory',          // 图书馆-分类
//     USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION:'libraryBookIntroduction',          // 图书馆-书籍介绍
//     USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION_ADD:'libraryBookAddToBookshelf',          // 图书馆-分类
//     USER_BEHAVIOR_LIBRARY_BOOKLIST:'libraryBookList',         // 图书馆-书单
//     USER_BEHAVIOR_LIBRARY_BOOKSHOP:'store',              // 图书馆-书店
//     USER_BEHAVIOR_LIBRARY_BOOKSHOP_MORE:'storeCategory',      // 图书馆-书店
//     USER_BEHAVIOR_LIBRARY_BOOKSHOP_DETAIL:'storeBookDetail',    // 图书馆-书店-书的详情介绍
//     USER_BEHAVIOR_LIBRARY_BOOKSHOP_BUY:'storeClickBuy',    // 图书馆-书店-去购买
//     USER_BEHAVIOR_LIBRARY_SEARCH:'librarySearch',           // 图书馆-搜索

//     USER_BEHAVIOR_FOLLOW:'follow',                                    // 订阅
//     USER_BEHAVIOR_CHAT:'chat',                                        // 答疑
//     USER_BEHAVIOR_ME:'user',                                            // 我
//     USER_BEHAVIOR_USER:'userLoginOrReg',                                  // 用户注册和登录
//     USER_BEHAVIOR_USER_REGBYMAIL:'userRegByMail',                       // 用户邮件注册
//     USER_BEHAVIOR_USER_REGBYPHONE:'userRegByPhone' ,                     // 用户手机注册

//     USER_BEHAVIOR_USER_OTHER:'user',                                  // 其他信息
//     USER_BEHAVIOR_USER_OTHER_SUBSCRIPTION:'userOpenSubscription',         // 订阅
//     USER_BEHAVIOR_USER_OTHER_FEEDBACK:'userClickFeedback',         // 其他信息
//     USER_BEHAVIOR_USER_OTHER_ARTICLE:'userOpenCollectionArtilce',           // 收藏
//     USER_BEHAVIOR_USER_OTHER_FRIDEND:'userShareToFriend',            // 分享给朋友

//     //用户点击事件跟踪
//     userAction(action,label,id) {
//         let event =  weex.requireModule('event');       
//         if(typeof(event.googleTrack)!='undefined'){
//             event.googleTrack(this.USER_BEHAVIOR,action,label,id);
//         }
//         console.log(action+" -- "+label+" -- "+id);
//     },
//     userHome() {
//         this.userAction(this.USER_BEHAVIOR_HOME,"",0);
//     },
//     userLibrary() {
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHELF,"",0);
//     },
//     //订阅
//     userFollow() {
//         this.userAction(this.USER_BEHAVIOR_FOLLOW,"",0);
//     },
//     //用户点击专栏
//     userOpenAlbum(albumName,albumId) {
//         this.userAction(this.USER_BEHAVIOR_ALBUM,albumName,albumId);
//     },
//     //用户点击文章次数
//     userOpenArticle(title,id) {
//         this.userAction(this.USER_BEHAVIOR_ARTICLE,title,id);
//     },
//     //点击阅读书
//     userLibraryOpenBook(title) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHELF,title,0);
//     },
//     //点击书籍分类
//     userLibraryOpenCategory(categoryName) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_CATEGORY,categoryName,0);
//     },
//     //点击书籍分类
//     userOpenBookIntroduction(bookName,bookId) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION,bookName,bookId);
//     },
//     //点击书籍分类
//     userOpenBookIntroductionAdd(bookName,bookId) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION_ADD,bookName,bookId);
//     },
//     //点击书单
//     userLibraryOpenBookList() { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKLIST,'',0);
//     },
//     //点击书单
//     userLibraryOpenBookListByName(name) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKLIST,name,0);
//     },
//     //点击书店导航
//     userLibraryOpenBookshop() { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP,"",0);
//     },
//      //点击书店中的更多
//      userLibraryOpenBookshopMore(categoryName) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP_MORE,categoryName,0);
//     },
//     //点击书店中某本书
//     userLibraryOpenBookshopDetails(bookName) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP_DETAIL,bookName,0);
//     },
//     //点击书店中去购买
//     userLibraryOpenBookshopBuy(bookName) { 
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP_BUY,bookName,0);
//     },
//     //点击搜索
//     userSearch(keywords) {
//         this.userAction(this.USER_BEHAVIOR_LIBRARY_SEARCH,keywords,0);
//     },
//     //点击答疑
//     userChat() {
//         this.userAction(this.USER_BEHAVIOR_CHAT,"",0);
//     },
//     //点击Me
//     userMe() {
//         this.userAction(this.USER_BEHAVIOR_ME,"",0);
//     },
//     //用户点击登录或注册按钮
//     userLoginOrReg() {
//         this.userAction(this.USER_BEHAVIOR_USER,"",0);
//     },
//     //用户用邮件注册
//     userRegByMail() {
//         this.userAction(this.USER_BEHAVIOR_USER,this.USER_BEHAVIOR_USER_REGBYMAIL,0);
//     },
//     //用户用手机注册
//     userRegByPhone() {
//         this.userAction(this.USER_BEHAVIOR_USER,this.USER_BEHAVIOR_USER_REGBYPHONE,0);
//     },
//     //订阅
//     userOtherSubscription() {
//         this.userAction(this.USER_BEHAVIOR_USER_OTHER,this.USER_BEHAVIOR_USER_OTHER_SUBSCRIPTION,0);
//     },
//     //收藏
//     userOtherArticle() {
//         this.userAction(this.USER_BEHAVIOR_USER_OTHER,this.USER_BEHAVIOR_USER_OTHER_ARTICLE,0);
//     },
//     //其他
//     userOtherFeedback() {
//         this.userAction(this.USER_BEHAVIOR_USER_OTHER,this.USER_BEHAVIOR_USER_OTHER_FEEDBACK,0);
//     },
//     userOtherShareToFriend() {
//         this.userAction(this.USER_BEHAVIOR_USER_OTHER,this.USER_BEHAVIOR_USER_OTHER_FRIDEND,0);
//     },
// };

// export default googleTrack;