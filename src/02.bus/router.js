import Router from 'vue-router'
import Vue from 'vue'

import me from '../03.components/wxme.vue'
import index from '../03.components/wxindex.vue'
import library from '../03.components/wxlibrary.vue'
import subscribe from '../03.components/wxsubscribe.vue'

Vue.use(Router)

export default new  Router({

    routes:[
        {path:'/me',component:me},
        {path:'/index',component:index},
        {path:'/library',component:library},  
        {path:'/subscribe',component:subscribe},  
    ]
});