const sync = new BroadcastChannel('book')

let syncBookShelf = {
  receive(fun) {
    sync.onmessage = fun;
  },
  addToShelf(bookId) {
    let message = {
      status:'add',
      bookId:bookId
    }
    sync.postMessage(message);
  },
  cancel() {
    let message = {
      status:'cancel',
    }
    sync.postMessage(message);
  },
  getData(){
    let message = {
      status:'getData',
    }
    sync.postMessage(message);
  },
  close(ablumnId,fun) {
    sync.close();
  }
};

export default syncBookShelf;