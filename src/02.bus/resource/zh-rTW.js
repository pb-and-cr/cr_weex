var lang = {
     "loading":"加載中",
     "networkError":"無法獲取網絡數據 請連接網絡後重試",
	 "refresh":"刷新",
	 "columnInfoItro":"專輯介紹",
	 "suggestion":"專欄推薦",
	 "alreadyFollow":"已訂閱專欄",
	 "videoplay":"視頻播放",
	 "follow":"訂閱",
	 "videofrom":"視頻來源",
	 "videoList":"本專欄視頻推薦",
	 "recommendation":"優秀專欄推薦",
	 "recommendedvideos":"優秀視頻推薦"
}
exports.lang = lang;