var lang = {
    "loading":"Loading",
    "networkError":"Unable to get network data, please connect to network and try again",
    "refresh":"Refresh",
    "columnInfoItro":"Album Introduction",
    "suggestion":"Recommended Album",
    "alreadyFollow":"Aready Follow",
    "videoplay":"Video Playing",
    "follow":"follow",
    "videofrom":"VideoFrom",
    "videoList":"Recommended Videos",
    "recommendation":"Excellent Column Recommendation",
    "recommendedvideos":"Excellent Video Recommendation"

}
exports.lang = lang;