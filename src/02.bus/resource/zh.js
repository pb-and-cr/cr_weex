var lang = {
    "loading":"加载中",
    "networkError":"无法获取网络数据 请连接网络后重试",
    "refresh":"刷新",
    "columnInfoItro":"专辑介绍",
    "suggestion":"专栏推荐",
    "alreadyFollow":"已订阅专栏",
    "videoplay":"视频播放",
    "follow":"订阅",
    "videofrom":"视频来源",
    "videoList":"本专栏视频推荐",
    "recommendation":"优秀专栏推荐",
    "recommendedvideos":"优秀视频推荐"
}
exports.lang = lang;