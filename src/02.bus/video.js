import {get,post} from '../01.utils/fetch'
import {getOpenId} from '../02.bus/user'
import global from '../02.bus/global'
import syncAblumInfo from '../02.bus/syncAblumInfo';

export function getVideoListInfo(pageindex,tagsId, fun) {
    let openId = getOpenId();
    return get('/api/video/v1/index', { 'openId': openId, 'pageindex': pageindex, 'tagsId': tagsId, 'language':global.getLanguage() }, fun);
}

export function getVideoInfoById(id, fun) {
    let openId = getOpenId();
    return get('/api/video/v1/play', { 'openId': openId, 'id': id, 'language':global.getLanguage()}, fun);
}
export function getFollowInfo(fun) {
    let openId = getOpenId();
    return get('/api/follow/v1/index', { 'openId': openId, 'language':global.getLanguage() }, fun);
}
//获得用户已经订阅的专栏信息：
export function getFollowColumnList(fun) {
    let openId = getOpenId();
    return get('/api/follow/v1/followColumnInfo', { 'openId': openId, 'language':global.getLanguage() }, fun);
}
//获得用户未订阅的专栏信息：
export function getNotsubscribedColumnInfo(fun) {
    let openId = getOpenId();
    return get('/api/follow/v1/notsubscribedList', { 'openId': openId, 'language':global.getLanguage() }, fun);
}
//用户订阅某个专栏
export function userFollow(columnId, fun){
    let openId = getOpenId();
    syncAblumInfo.follow(columnId);
    return get('/api/follow/v1/userFollow', { 'columnId': columnId, 'openId': openId, 'language':global.getLanguage() }, fun);
}
//用户取消某个专栏的订阅：
export function userRemoveColumn(columnId, fun){
    let openId = getOpenId();
    syncAblumInfo.cancel(columnId);
    return get('/api/follow/v1/userRemoveColumn', { 'columnId': columnId, 'openId': openId, 'language':global.getLanguage() }, fun);
}
//根据专栏id获得专栏详细信息
export function getColumnInfo(columnId, fun){
  let openId = getOpenId();
  return get('/api/follow/v1/getColumnInfo', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}
