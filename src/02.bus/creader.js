import { get, post } from '../01.utils/fetch'
import { getOpenId, getUserId, setUserInfo } from '../02.bus/user'
import { storageSetItem, storageGetItem, storageRemoveItem } from '../01.utils/storage'
import global from '../02.bus/global'
import syncAblumInfo from '../02.bus/syncAblumInfo';
import prompt from '../01.utils/prompt';
import { jumpSubPage } from '../01.utils/navigator';
// import googleTrack from '../02.bus/googleTrack';

const BOOK_SHELF = 'BookShelf';

//用户获取书架信息
export function getBookShelf() {
    //登陆成功后返回token，和user_id
    //let openId = getOpenId();
    //let user_id = getUserId();
    return storageGetItem(BOOK_SHELF).then(res => {
        // console.log(res.data);
        if (res.result == "failed") {
            return [];
        }
        else {
            let data = JSON.parse(res.data)
            let len = data.length;
            for (let i = 0; i < len; i++) {
                data[i]['progress'] = { "visible": false, "value": 0 };
            }
            console.log(data);
            return data;
        }
    })
}

//用户获取书架信息
export function saveBookShelf(bookList) {
    //登陆成功后返回token，和user_id
    return storageSetItem(BOOK_SHELF, JSON.stringify(bookList));
}

export function openBook(bookInfo) {

}

//从服务器端同步数据到本地
export function getBookListByService(bookList, pageIndex) {
    return getUserId().then(userId => {
        return get('/api/Books/getBooksOnShelf', { 'user_id': userId, "page_index": pageIndex });
    })
}

export function getArticleListInfo(pageIndex) {
    return getOpenId().then(openId => {
        return get('/api/Article/v1/index', { 'openId': openId, 'pageIndex': pageIndex });
    })
}

export function getArticleListByBrowse(pageIndex) {
    return getOpenId().then(openId => {
        return get('/api/Article/v1/getArticleListByBrowse', { 'openId': openId, 'pageIndex': pageIndex });
    })
}
export function getSubscribeInfo() {
    return getOpenId().then(openId => {
        return get('/api/follow/v1/index', { 'openId': openId });
    })
    // return get('/api/follow/v1/index', { 'openId': openId}, fun);
}
export function queryAllAlbumLists() {
    return getOpenId().then(openId => {
        return get('/api/follow/v1/queryAllAlbumLists', { 'openId': openId });
    })
}
export function getAlbumInfoById(id, pageIndex) {
    return getOpenId().then(openId => {
        return get('/api/Article/v1/album', { 'openId': openId, 'pageIndex': pageIndex, 'id': id });
    })
    // return get('/api/Article/v1/album', { 'openId': openId, 'id': id,'pageIndex':pageIndex}, fun);
}
//获取图书信息
export function getBooks(id, pageIndex) {
    return getOpenId().then(openId => {
        return get('/api/Books/getBooksTags', { 'openId': openId, 'id': id, 'pageIndex': pageIndex });
    })
    // return get('/api/Books/getBooksTags',{'openId': openId,'id': id,'pageIndex':pageIndex},fun);
}
//获取书籍信息
export function getBookProfile(id) {
    return getOpenId().then(openId => {
        return get('/api/Books/getBookProfile', { 'openId': openId, 'id': id });
    })
}
//用户添加书籍到书架
export function userAddBookToShelf(bookId) {
    //登陆成功后返回token，和user_id
    //let openId = getOpenId();
    //let user_id = getUserId();
    // let user_id = 7245;
    //type = 1 表示书籍
    return getUserId().then(userId => {
        return get('/api/Books/addBookToShelf', { 'article_book_id': bookId, 'user_id': userId, 'type': 1 });
    })
    // return get('/api/Books/addBookToShelf', { 'book_id': bookId, 'user_id': user_id}, fun);
}
//用户从书架移出书籍
export function userRemoveBookToShelf(bookId) {
    //登陆成功后返回token，和user_id
    //let openId = getOpenId();
    //let user_id = getUserId();
    // let user_token = 7245;
    // return get('/api/Books/removeBookFromShelf', { 'book_id': bookId, 'user_token': user_token}, fun);
    return getUserId().then(userId => {
        console.log(" userId -  " + userId);
        if (userId <= 0) {
            return { data: { status: 1 } }
        }
        else {
            return get('/api/Books/removeBookFromShelf', { 'article_book_id': bookId, 'user_id': userId, 'type': 1 }).then(res => {
                if (userId <= 0) {
                    return { data: { status: 1 } }
                }
                else {
                    return res;
                }
            });
        }
    })
}
//用户搜索图书
export function serchBookName(name, pageIndex) {
    return getUserId().then(userId => {
        return post('/api/Books/searchBook', { 'title': name, 'pageIndex': pageIndex })
    })
}

//用户搜素文章，图书，专栏
export function searchResult(value, type, pageIndex) {
    //type = 1,查询书;2表示查询文章;3，表示查询专辑
    return getUserId().then(userId => {
        return post('/api/follow/searchResult', { 'title': value, 'pageIndex': pageIndex, 'type': type, 'user_id': userId })
    })
}
//用户添加文章到我的收藏
export function userAddAticleToLove(articleId, loveCount) {
    //type = 2 表示文章
    return getUserId().then(userId => {
        if (userId == -1) {
            return -1;
        } else {
            return get('/api/Article/addArticleToLove', { 'article_book_id': articleId, 'love_count': loveCount, 'user_id': userId, 'type': 2 });
        }
    })
}
//用户移出文章到我的收藏
export function userRemoveAticleFromLove(articleId, loveCount) {
    return getUserId().then(userId => {
        return get('/api/Article/removeArticleFromLove', { 'article_book_id': articleId, 'love_count': loveCount, 'user_id': userId });
    })
}
export function isArticleInLove(articleId) {
    return getUserId().then(userId => {
        return get('/api/Article/isArticleInLove', { 'article_book_id': articleId, 'user_id': userId });
    })
}
//用户获取收藏的文章信息
export function userGetLovedArticle(pageIndex) {
    //type = 2 表示文章
    return getUserId().then(userId => {
        return get('/api/Article/articleInLove', { 'user_id': userId, 'pageIndex': pageIndex, 'type': 2 });
    })
}
//用户阅读文章，获取文章的相关信息
export function getArticleInfoById(id) {
    return getUserId().then(userId => {
        return get('/api/Article/articleInfoById', { 'id': id, 'user_id': userId })
    })
}

//用户阅读文章，获取文章的相关信息
export function getArticleOtherInfo(id, album_id) {
    return getUserId().then(userId => {
        return get('/api/Article/getArticleOtherInfo', { 'id': id, 'user_id': userId, 'album_id': album_id })
    })
}

//用户分享文章
export function shareArticleToFriends(id) {
    return getUserId().then(userId => {
        return get('/api/Article/shareArticles', { 'id': id })
    })
}


//获取书单
export function getSuggestBookLists() {
    // let openId = getOpenId();
    return getOpenId().then(openId => {
        return get('/api/Books/getSuggestBookLists', { 'openId': openId });
    })
    // return get('/api/Books/getSuggestBookLists',{'openId': openId},fun);
}
//获取书单下的详细书本信息
export function getBookListsInfo(bookList_id) {
    // let openId = getOpenId();
    return getOpenId().then(openId => {
        return get('/api/Books/getBookListsInfo', { 'openId': openId, 'bookList_id': bookList_id });
    })
    // return get('/api/Books/getBookListsInfo',{'openId': openId,'bookList_id':bookList_id},fun);
}
//用户获取个人信息
export function getPersonalData() {
    // let openId = getOpenId();
    // let userId = getUserId();
    return getOpenId().then(openId => {
        return get('/api/ChristianReaderUser/getPersonalData', { 'openId': openId }).then(res => {
            if (res.data.status == 1) {
                setUserInfo(res.data.data);
            }
            else {
                setUserInfo(res.data.data);
            }
            return res;
        });
    })
    // return getPromise('/api/ChristianReaderUser/getPersonalData',{'openId': openId});
    // return get('/api/ChristianReaderUser/getPersonalData',{'openId': openId},fun);
}

//获得用户已经订阅的专栏信息：
export function getFollowColumnList() {
    return getOpenId().then(openId => {
        return get('/api/follow/v1/followColumnInfo', { 'openId': openId });
    })
    // return get('/api/follow/v1/followColumnInfo', { 'openId': openId}, fun);
}
//获得用户未订阅的专栏信息：
export function getNotsubscribedColumnInfo() {
    return getOpenId().then(openId => {
        return get('/api/follow/v1/notsubscribedList', { 'openId': openId });
    })
    // return get('/api/follow/v1/notsubscribedList', { 'openId': openId}, fun);
}
//用户订阅某个专栏
export function userFollow(columnId) {
    return getOpenId().then(openId => {
        return get('/api/follow/v1/userFollow', { 'columnId': columnId, 'openId': openId });
    })
    // syncAblumInfo.follow(columnId);
    // return get('/api/follow/v1/userFollow', { 'columnId': columnId, 'openId': openId}, fun);
}
//用户取消某个专栏的订阅：
export function userRemoveColumn(columnId) {
    return getOpenId().then(openId => {
        return get('/api/follow/v1/userRemoveColumn', { 'columnId': columnId, 'openId': openId });
    })
    // syncAblumInfo.cancel(columnId);
    // return get('/api/follow/v1/userRemoveColumn', { 'columnId': columnId, 'openId': openId}, fun);
}
//根据专栏id获得专栏详细信息
export function getColumnInfo(columnId) {
    return getOpenId().then(openId => {
        return get('/api/follow/v1/getColumnInfo', { 'columnId': columnId, 'openId': openId });
    })
    // return get('/api/follow/v1/getColumnInfo', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}
export function getBookList() {
    return getOpenId().then(openId => {
        return get('/api/shop/getBooksInShop', { 'openId': openId });
    })
    // return get('/api/shop/getBooksInShop', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}
export function getBookListByTag(tagid) {
    return getOpenId().then(openId => {
        return get('/api/shop/getBooksInShopByTag', { 'openId': openId, 'tag_id': tagid });
    })
    // return get('/api/shop/getBooksInShop', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}

