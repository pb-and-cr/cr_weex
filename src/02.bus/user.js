import { get, post, getHost } from '../01.utils/fetch'
import { storageSetItem, storageGetItem, storageRemoveItem } from '../01.utils/storage'
import global from '../02.bus/global'
import syncAblumInfo from '../02.bus/syncAblumInfo'

const syncUser = new BroadcastChannel('christianReaderUser')
const USER_KEY = 'userKey';  //用户存储的KeyValue

let syncUserInfo = {
    receive(fun) {
        syncUser.onmessage = function (event) {
            // console.log(event.data) // Assemble!
            fun();
        }
    },
    post() {
        let message = {
            status: 'update',
        }
        syncUser.postMessage(message);
    },
    close(fun) {
        syncUser.close();
    }
};

export default syncUserInfo;

export function getOpenId() {
    return storageGetItem(USER_KEY).then(res => {
        if (res.result == "failed") {
            return "";
        }
        else {
            console.log("openId: " + res.data);
            return JSON.parse(res.data).openId;
            // return "8A471FD74AF017EE0680381611D442CD";
        }
        // return "8A471FD74AF017EE0680381611D442CD";
    });
    // return "8A471FD74AF017EE0680381611D442CD";
}

export function getUserId() {
    return storageGetItem(USER_KEY).then(res => {
        if (res.result == "failed") {
            return -1;
        }
        else {
            console.log("openId: " + res.data);
            return JSON.parse(res.data).id;
        }
    });
}

export function getUserInfo() {
    return storageGetItem(USER_KEY).then(res => {

        if (res.result == "failed") {
            return {
                id: -1,
                openId: '',
                head_icon: getHost() + '/static/default/2.jpg',
                nickName: '主的好孩子,没有任何信息',
                profile: '点击这里登录或注册',
                openId: '',
                bookCounts: 0,
                loveCounts: 0,
                followCounts: 0
            };
        }
        else {
            return JSON.parse(res.data);
        }
    });
}

//更新资料
export function exit() {
    storageRemoveItem(USER_KEY);
}

export function setUserInfo(value) {
    return storageSetItem(USER_KEY, JSON.stringify(value));
}


//登陆
export function login(userName, password) {
    return post('/api/ChristianReaderUser/login', { 'userName': userName, 'password': password }).then(res => {
        console.log(" -- login -- ");
        console.log(res);
        if (res.data.status == 1) {
            setUserInfo(res.data.data);
        }
        return res;
    });
    // return post('/api/ChristianReaderUser/login', { 'userName': userName, 'password': password}, fun);
}
//用户邮箱注册
export function registerByEmail(nickName, email, password) {
    return post('/api/ChristianReaderUser/registerByEmail', { 'nickName': nickName, 'email': email, 'password': password });
    // return post('/api/ChristianReaderUser/registerByEmail', { 'nickName': nickName ,'email': email,'password': password}, fun);
}
//用户手机注册
export function registerByPhone(nickName, mobile, verifyCode, password) {
    return post('/api/ChristianReaderUser/registerByPhone', { 'nickName': nickName, 'mobile': mobile, 'verifyCode': verifyCode, 'password': password });
    // return post('/api/ChristianReaderUser/registerByPhone', { 'nickName': nickName ,'mobile': mobile,'verifyCode': verifyCode,'password': password}, fun);
}
//找回密码
export function getPswBackByEmail(email) {
    return post('/api/ChristianReaderUser/getPasswordBack', { 'email': email });
    // return post('/api/ChristianReaderUser/getPasswordBack', { 'email': email}, fun);
}
export function getPswBackByPhone(mobile, verifyCode) {
    return post('/api/ChristianReaderUser/getPasswordBackByPhone', { 'mobile': mobile, 'verifyCode': verifyCode });
    // return post('/api/ChristianReaderUser/getPasswordBackByPhone', { 'mobile': mobile, 'verifyCode': verifyCode,}, fun);
}
//发送手机验证码
export function sendPhoneCode(mobile) {
    return post('/api/ChristianReaderUser/getSms', { 'mobile': mobile });
}
//修改密码
export function updatePsw(mobile, password) {
    return post('/api/ChristianReaderUser/updatePswByPhone', { 'mobile': mobile, 'password': password });
}
//更新资料
export function updateProfile(name, nickName, des) {
    return getOpenId().then(openId => {
        return post('/api/ChristianReaderUser/updatePersonalProfile',
            { 'name': name, 'nickName': nickName, 'brief_personal_des': des, 'user_token': openId }).then(res => {
                if (res.data.status == 1) {
                    getUserInfo().then(res => {
                        res.nickName = nickName;
                        res.profile = des;
                        setUserInfo(res);
                    })
                }
                return res;
            });
    })
}
//用户反馈
export function userFeedback(contactWays, content) {
    return post('/api/ChristianReaderUser/feedback', { 'ContactInformation': contactWays, 'Content': content, 'AppInfo': '基督徒阅读' });
}

