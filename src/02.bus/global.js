let environment = {
    language:"zh" //改变这里的值。来切换中英文
}

let langFiles = {
    en:require("./resource/en.js").lang,
    zh:require("./resource/zh.js").lang,
    zh_tw:require("./resource/zh-rTW.js").lang
}

let already = false;

let global = {

    init:function(){
      if(already==true){
        return;
      }
      console.log(" global init ");
      let platform = weex.config.env.platform;
      let lang = "zh";
      try {
        let deviceInfo = weex.requireModule('deviceInfo');
        if (typeof(deviceInfo) != "undefined" && typeof(deviceInfo.GetLanguage) == "function") {
          lang = deviceInfo.GetLanguage().toLocaleLowerCase();
        }
      }
      catch (e) {
        console.log(" lang "+e);
      }

      if(platform.toLocaleLowerCase() == "ios"){
            // # ios #
            // 中文简体：zh-Hans
            // 英文：en
            // 中文繁体：zh-Hant
            // 印地语：hi
            // 西班牙语：es
            // 墨西哥西班牙语：es-MX
            // 拉丁美洲西班牙语：es-419
            if (lang.indexOf("zh") >= 0 ) {
               if(lang.indexOf("hans") >= 0){
                 environment.language = "zh";
               }
               else
               {
                 environment.language = "zh_tw";
               }
            }
            else  if (lang.indexOf("en") >= 0 ) {
              environment.language = "en";
            }else if (lang.indexOf("es") >= 0) {
              environment.language = "en";
            }
            else {
              environment.language = "zh";
            }
      }
      else {
            if (lang.indexOf("zh_cn") >= 0 || lang.indexOf("zh_cn") >= 0) {
              environment.language = "zh";
            } else if (lang.indexOf("zh_") >= 0) {
              environment.language = "zh_tw";
            } else if (lang.indexOf("cn") >= 0 || lang.indexOf("cn") >= 0) {
              environment.language = "zh";
            }
            else if (lang.indexOf("en") >= 0) {
              environment.language = "en";
            }
            else {
              environment.language = "zh";
            }
      }
    },
    lang:function(key) {
        return "";
    },
    display:function(key){
        if(already==false){
          this.init();
        }
        console.log(" already "+already);
        if (environment.language == "zh") {
            return langFiles.zh[key] || "not find";
        }
        else if (environment.language == "en") {
            return langFiles.en[key] || "not find";
        }
        else if (environment.language == "zh-tw") {
            return langFiles.zh_tw[key] || "not find";
        }
        return key;
    },
    getLanguage:function(){
        if(already==false){
          this.init();
        }
        return environment.language
    }
}

export default global;
//
// setTimeout(function(){
//     exports.lang = function(key) {
//         if (environment.language == "zh") {
//             return langFiles.zh[key] || "not find";
//         }
//         else if (environment.language == "en") {
//             return langFiles.en[key] || "not find";
//         }
//         else if (environment.language == "zh-tw") {
//             return langFiles.zh_tw[key] || "not find";
//         }
//         return key;
//     }
// },100);