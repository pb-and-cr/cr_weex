import { getImgPath } from '../../01.utils/common';
export default {
  tabItems:[
      {
          index:0,
          title:"主页",  
          titleColor:'#666666',  
          selectedColor: '#E15D53',
          fontSize: '25px',
          iconWdith: '48px',
          iconHeight: '48px',
          iconFont:'ion-android-home',
          image:getImgPath("home.png"),  
          selectedImage:getImgPath('homeselected.png'),  
          ref:'home',
      },  
      {
          index:1,  
          title:"专栏",  
          titleColor:'#666666',  
          selectedColor: '#E15D53',
          fontSize: '25px',
          iconWdith: '48px',
          iconHeight: '48px',
          iconFont:'ion-help-buoy',
          image:getImgPath("library.png"),  
          selectedImage:getImgPath("libraryselected.png"), 
          ref:'album', 
      },  
    //   {
    //       index:2,  
    //       title:"消息",   
    //       titleColor:'#666666',  
    //       selectedColor: '#E15D53',
    //       fontSize: '25px',
    //       iconWdith: '48px',
    //       iconHeight: '48px',
    //       iconFont:'ion-chatboxes',
    //       image:getImgPath("subscribe.png"),  
    //       selectedImage:getImgPath("subscribeselected.png"),  
    //       ref:'chat',
    //   },
      {
          index:2,  
          title:"图书",   
          titleColor:'#666666',  
          selectedColor: '#E15D53',
          fontSize: '25px',
          iconWdith: '48px',
          iconHeight: '48px',
          iconFont:'ion-ios-book',
          image:getImgPath("chat.png"),  
          selectedImage: getImgPath("chatselected.png"), 
          ref:'books', 
      } ,
      {
          index:3,  
          title:"我",   
          titleColor:'#666666',  
          selectedColor: '#E15D53',
          fontSize: '25px',
          iconWdith: '48px',
          iconHeight: '48px',
          iconFont:'ion-ios-person',
          image:getImgPath("me1.png"),  
          selectedImage:getImgPath("meselected.png"),  
          ref:'me',
      } , 
    ],
    bookstoremessage :{message:"来自合作伙伴的精选书籍推荐给您"},
    bottommessage :{message:"没有更多了!"},

  // 正常模式的tab title配置
  // tabTitles: [
  //   {
  //     title: '主页',
  //     icon: getImgPath("home.png"),
  //     activeIcon: getImgPath('homeselected.png'),
  //   },
  //   {
  //     title: '图书馆',
  //     icon: getImgPath("library.png"),
  //     activeIcon: getImgPath("libraryselected.png")
  //   },
  //   {
  //     title: '订阅',
  //     icon: getImgPath("subscribe.png"),
  //     activeIcon: getImgPath("subscribeselected.png")
  //   },
  //   {
  //     title: '答疑',
  //     icon: getImgPath("chat.png"),
  //     activeIcon: getImgPath("chatselected.png")
  //   },
  //   {
  //     title: '我',
  //     icon: getImgPath("me1.png"),
  //     activeIcon: getImgPath("meselected.png")
  //   }
  // ],
  // tabStyles: {
  //   bgColor: '#FFFFFF',
  //   titleColor: '#666666',
  //   activeTitleColor: '#E15D53',
  //   activeBgColor: '#ffffff',
  //   isActiveTitleBold: true,
  //   iconWidth: 60,
  //   iconHeight: 60,
  //   width: 160,
  //   height: 120,
  //   BottomColor: '#FFC900',
  //   BottomWidth: 0,
  //   BottomHeight: 0,
  //   activeBottomColor: 'green',
  //   activeBottomWidth: 160,
  //   activeBottomHeight: 6,
  //   fontSize: 24,
  //   textPaddingLeft: 10,
  //   textPaddingRight: 10
  // },
}  

